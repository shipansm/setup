if ( ! function_exists('show_404'))
{
	/**
	 * 404 Page Handler
	 *
	 * This function is similar to the show_error() function above
	 * However, instead of the standard error template it displays
	 * 404 errors.
	 *
	 * @param	string
	 * @param	bool
	 * @return	void
	 */
	function show_404(){
		$ci = get_instance();
		$ci->output->set_status_header(404);
		$ci->load->view('err404');
		echo $ci->output->get_output();
		exit(4);
	}
}