<?php

header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('logged_in_user_id')) {

    function logged_in_user_id() {
        $logged_in_id = 0;
        $CI = & get_instance();
        if ($CI->session->userdata('id')):
            $logged_in_id = $CI->session->userdata('id');
        endif;
        return $logged_in_id;
    }

}
if (!function_exists('logged_in_name')) {

    function logged_in_name() {
        $logged_in_name = '';
        $CI = & get_instance();
        if ($CI->session->userdata('id')):
            if (logged_in_role_name() == 'Super Admin'):
                $logged_in_name="Super Admin";
            else:
                $name=$CI->db->get_where("employee",array("user_id"=>$CI->session->userdata('id')))->row_array()['name'];
                $logged_in_name = $name;
            endif;
        endif;
        return $logged_in_name;
    }

}

if (!function_exists('setMessage')) {

    function setMessage($key,$class,$message) {
        $CI = & get_instance();
        $CI->session->set_flashdata($key,'<div class="alert alert-'.$class.'">'.$message.'</div>');
        return true;
    }

}
if (!function_exists('active_link')) {

    function set_Topmenu($top_menu_name) {
        $CI = get_instance();
        $session_top_menu = $CI->session->userdata('top_menu');
        if ($session_top_menu == $top_menu_name) {
            return 'active';
        }
        return "";
    }

    function set_Submenu($sub_menu_name) {
        $CI = get_instance();
        $session_sub_menu = $CI->session->userdata('sub_menu');
        if ($session_sub_menu == $sub_menu_name) {
            return 'active';
        }
        return "";
    }

}
if(!function_exists("set_loging_agent")){
    function set_loging_agent()
    {
        $CI =& get_instance();
        
        $CI->load->library('user_agent');

		if ($CI->agent->is_browser())
		{
				$agent = $CI->agent->browser().' '.$CI->agent->version();
		}
		elseif ($CI->agent->is_robot())
		{
				$agent = $CI->agent->robot();
		}
		elseif ($CI->agent->is_mobile())
		{
				$agent = $CI->agent->mobile();
		}
		else
		{
				$agent = 'Unidentified User Agent';
		}

		return $agent.$CI->agent->platform();
    }
    
}
if(!function_exists("setting_info")){
    function setting_info()
    {
        $CI =& get_instance();
        
        $CI->db->select('*');
        $CI->db->from('setting');
        $CI->db->order_by('id', 'desc');
        $CI->db->limit(1);
        return $CI->db->get()->row();
    }
}
if(!function_exists("breadcrumbs"))
{
    function breadcrumbs($home = 'Home')
    {
        global $page_title; //global varable that takes it's value from the page that breadcrubs will appear on. Can be deleted if you wish, but if you delete it, delete also the title tage inside the <li> tag inside the foreach loop.
        $breadcrumb  = '<ol class="breadcrumb pull-right">';
        $root_domain = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
        $breadcrumbs = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
        // $breadcrumb .= '<li><i class="fa fa-home"></i><a href="' . $root_domain . '" title="Home Page"><span>' . $home . '</span></a></li>';
        foreach ($breadcrumbs as $crumb) {
            $link = ucwords(str_replace(array(".php", "-", "_"), array("", " ", " "), $crumb));
            $root_domain .=  $crumb . '/';
            $breadcrumb .= '<li><a href="' . $root_domain . '" title="' . $page_title . '"><span>' . $link . '</span></a></li>';
        }
        $breadcrumb .= '</ol>';
        return $breadcrumb;
    }
}
