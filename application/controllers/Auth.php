<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* * ***************Auth.php**********************************
 * @product name    : Global School Management System Pro
 * @type            : Class
 * @class name      : Auth
 * @description     : This class used to handle user authentication functionality 
 *                    of the application.  
 * @author          : Codetroopers Team 	
 * @url             : https://themeforest.net/user/codetroopers      
 * @support         : yousuf361@gmail.com	
 * @copyright       : Codetroopers Team	 	
 * ********************************************************** */

class Auth extends CI_Controller {

    public $data = array();

    public function __construct() {

        parent::__construct();
        $setting=setting_info();
        date_default_timezone_set($setting->time_zone);
        $this->load->model('Auth_model', 'auth', true);
    }

    /** ***************Function login**********************************
     * @type            : Function
     * @function name   : login
     * @description     : Authenticatte when uset try lo login. 
     *                    if autheticated redirected to logged in user dashboard.
     *                    Also set some session date for logged in user.   
     * @param           : null 
     * @return          : null 
     * ********************************************************** */

    public function login() {
        if(logged_in_user_id())
        {
            redirect('dashboard');
        }
        if ($_POST) {
            $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger">Validation Error!</div>');
            }
            else
            {
                $username = $this->input->post('username');
                $password =$this->input->post('password');
                $check_username=$this->auth->get_single("admin",array("username"=>$username));
                if($check_username)
                {
                    if($check_username->status==1)
                    {
                        $this->load->library("Enc_lib");
                        $password_verify=$this->enc_lib->passHashDyc($password,$check_username->password);
                        if($password_verify)
                        {
                            if(!$check_username->status==1)
                            {
                                $this->session->set_flashdata('msg', '<div class="alert alert-warning">Your Account Not Active.</div>');
                                redirect();    
                            }
                            $role=$this->auth->get_single("roles",array("id"=>$check_username->role_id));
                            if(empty($role)){
                                $this->session->set_flashdata('msg', '<div class="alert alert-warning">You have no permission</div>');
                                redirect();
                            }
                            if($role->id!=1)
                            {
                                $permission=$this->auth->get_list("roles_permissions",array("role_id"=>$check_username->role_id));
                                if (empty($permission)) {
                                    $this->session->set_flashdata('msg', '<div class="alert alert-warning">You have no permission</div>');
                                    redirect();
                                }
                            }
                            $this->session->set_userdata('id', $check_username->id);
                            $this->session->set_userdata('username', $check_username->username);
                            $this->session->set_userdata('role_id', $check_username->role_id);
                            $this->session->set_userdata('role_name', $role->name);
                            $log_data=array(
                                "ip"=>$_SERVER['REMOTE_ADDR'],
                                "user_agent"=>set_loging_agent(),
                                "last_login"=>date("Y-m-d H:i:s")
                            );
                            $this->auth->update("admin",$log_data,array("id"=>$check_username->id));
                            $this->session->set_flashdata('msg', '<div class="alert alert-success">Login Successfully.</div>');
                            redirect('dashboard');
                        }
                        else
                        {
                            $this->session->set_flashdata('msg', '<div class="alert alert-danger">Password Incorrect!</div>');
                            redirect();
                        }
                    }else{
                        $this->session->set_flashdata('msg', '<div class="alert alert-danger">Your Account Disabled!</div>');    
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger">Username No Matched!</div>');
                    redirect();
                }
            }
        }
        $this->load->view("login");
    }

    /*     * ***************Function logout**********************************
     * @type            : Function
     * @function name   : logout
     * @description     : Log Out the logged in user and redirected to Login page  
     * @param           : null 
     * @return          : null 
     * ********************************************************** */

    public function logout() {
        if(!logged_in_user_id())
        {
            redirect('');
        }
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('location_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('name');

        $this->session->sess_destroy();
        redirect();
        exit;
    }

}
