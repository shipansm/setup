<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {
    public $data=array();
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Ajax_model",'ajax',true);
    }
    
    public function index()
    {
        
    }
}

/* End of file Ajax.php */
