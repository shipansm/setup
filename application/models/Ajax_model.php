<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_model extends MY_Model {
    public function get_batch_id($table, $prefix)
    {
        $max_id = '';
        $this->db->select_max('id');
        $max_id = $this->db->get($table)->row()->id;

        if (isset($max_id) && $max_id > 0) {
            $max_id = $max_id + 1;
        } else {
            $max_id = 1;
        }

        return $prefix.str_pad($max_id, 4, 0, STR_PAD_LEFT);
    }

    public function get_student_id($batch_id)
    {
        $this->db->select('S.id as st_id,S.student_unique_id');
        $this->db->from('batch_log as BL');
        $this->db->join('student as S', 'BL.student_id = S.id');
        $this->db->where('BL.batch_id', $batch_id);
        return $this->db->get()->result_array();
    }
    public function get_student_amount($batch_id,$student_id)
    {
        $this->db->select('C.course_fee');
        $this->db->from('batch_log as BL');
        $this->db->join('batch as B', 'BL.batch_id = B.id');
        $this->db->join('course_details as C', 'B.course_id = C.course_id');
        $this->db->where("BL.batch_id",$batch_id);
        $this->db->where("C.running_session",$this->running_session);
        $result=$this->db->get()->row_array();
        if(isset( $result))
        {
           $data=$this->get_student_due($batch_id, $student_id);
           $result["total_due"]= $result['course_fee']-$data['total_discount']-$data[ 'total_pay'];
        
        }
        return $result;
    }  
    public function get_student_due($batch_id, $student_id)
    {
        $this->db->select( 'SUM(P.pay) as total_pay,SUM(P.discount) as total_discount');
        $this->db->from('payment as P');
        $this->db->where('P.batch_id', $batch_id);
        $this->db->where("P.running_session",$this->running_session);
        $this->db->where('P.student_id', $student_id);
        return $this->db->get()->row_array();
    }
    public function get_student_invoice($batch_id=null, $student_id=null)
    {
        $this->db->select('C.course_unique_id,P.*,P.id as p_id,B.batch_unique_id,S.student_name,S.student_unique_id,S.id as st_id');
        $this->db->from('payment as P');
        $this->db->join('batch as B', 'P.batch_id = B.id');
        $this->db->join('course as C', 'B.course_id = C.id');
        $this->db->join('student as S', 'S.id = P.student_id');
        $this->db->order_by("P.id","desc");
        if($batch_id!='')
        {
            $this->db->where("P.batch_id", $batch_id);
        }
        if($student_id!='')
        {
            $this->db->where("P.student_id", $student_id);
        }
        $this->db->where("P.running_session", $this->running_session);
        $result = $this->db->get()->result_array();
        return $result;
    }  
    public function get_student_by_session($session_id)
    {
        $this->db->select('S.student_unique_id,S.id as st_id');
        $this->db->from('student_enrollment as SE');
        $this->db->join('student as S', 'SE.student_id = S.id');
        $this->db->order_by('S.student_unique_id', 'ASC');
        $this->db->where('SE.running_session', $session_id);
        return $this->db->get()->result_array();
    }
    public function get_course_by_session($session_id)
    {
        $this->db->select('C.course_unique_id,C.id as c_id');
        $this->db->from('course_details as CD');
        $this->db->join('course as C', 'CD.course_id = C.id');
        $this->db->order_by('C.course_unique_id', 'ASC');
        $this->db->where('CD.running_session', $session_id);
        return $this->db->get()->result_array();
    }
    public function get_student_list($batch_id, $session_id)
    {
        $this->db->select('S.*,S.id as st_id,B.course_id,SD.*,SE.*,SE.status as student_status');
        $this->db->from('batch_log as BL');
        $this->db->join('batch as B', 'BL.batch_id = B.id');
        $this->db->join('student as S', 'BL.student_id = S.id', "right");
        $this->db->join('student_details as SD', 'S.id = SD.student_id');
        $this->db->join('student_enrollment as SE', 'S.id = SE.student_id');
        $this->db->group_by('S.student_unique_id');
        $this->db->where('BL.batch_id', $batch_id);
        $this->db->where('BL.running_session', $session_id);
        $this->db->where('SE.status',1);
        return $this->db->get()->result_array();
    }
    public function get_student_certificate_list($batch_id, $session_id,$certificate_id=null)
    {
        $this->db->select( 'S.*,C.student_id as st_id,B.course_id,C.grade,C.status,C.id as certificate_id');
        $this->db->from('certificate as C');
        $this->db->join('student as S', 'C.student_id = S.id',"left");
        $this->db->join('batch as B', 'C.batch_id = B.id',"left");
        $this->db->join('student_enrollment as SE', 'S.id = SE.student_id');
        $this->db->where('SE.running_session', $session_id);
        $this->db->where('SE.status',1);
        $this->db->where('C.batch_id', $batch_id);
        $this->db->order_by('S.student_name',"ASC");
        return $this->db->get()->result_array();
    }
    public function get_max($table)
    {
        $this->db->select_max('id');
        return $this->db->get($table)->row()->id;
    }
}

/* End of file Ajax.php */
